A part of Event Index https://eindex.cern.ch/

# Manual usage


## Formulate a request in json format
```
{<lfn>: {
   "positions": [position_1, position_2, ...],
   "cond_db": {"DDDB": <>, "ONLINE": <>, "DQFLAGS": <>, "LHCBCOND": <>}
   },n
   ...
   }

```
   See examples/small.json for example. Also please note that
   cond_db fields must be present. If you don't know them,
   you can leave them blank - in this case there are no guarantees on the quality of the result,
   please refer to InputCopyStream and JsonConverter documentation.

## Retrieve LFN->PFN mapping

1. Run ```SetupProject LHCbDIRAC```
2. Run ```resolve_lfns.py request lfn_map_file```, for exmaple ```resolve_lfns.py example/small.json example/small_lfn_map.json```
If you see "No proxy found", run ```voms-proxy-init -voms lhcb```  

## Fetch the events
1. Run ```SetupProject DaVinci```. If you want json files for EventDisplay be sure to have
the JsonConverter algorithm included in your DaVinci build. If not, edit grid_wizard.py
to exclude JsonConverter.
2. Run

```
run_process_request.py [-h] [-n N_JOBS]
                              request_file lfn_catalog status_file
                              request_folder

Runs the event retrieval and root to json convertion for Event Index

positional arguments:
  request_file          Event Index request file to run on
  lfn_catalog           .json for LFNResolver
  status_file           file to dump request status
  request_folder        folder for storing jobs

optional arguments:
  -h, --help            show this help message and exit
  -n N_JOBS, --n-jobs N_JOBS
                        number of parallel jobs to launch
```

After the run, request_folder will contain individual folders with .root and .json files
for each lfn and status_file will contain a list of all lfns and folders corresponding to them.

Example: ```python run_process_request.py example/small.json example/small_lfn_map.json example/small_status.json example/data -n 10```
## Collecting the request results
```
usage: collector.py [-h] [-c] [-j JSON_FOLDER] [-u URL_PREFIX] [-r ROOT_FILE]
                    status_file request_folder

Collects processed request results for EventIndex

positional arguments:
  status_file           request status file
  request_folder

optional arguments:
  -h, --help            show this help message and exit
  -c, --cleanup         Clean the job folders after the merge
  -j JSON_FOLDER, --json-folder JSON_FOLDER
                        If specified, move EventDisplay json files to the
                        folder
  -u URL_PREFIX, --url-prefix URL_PREFIX
                        Add this prefix to the json files listing. Meaningless
                        without -j
  -r ROOT_FILE, --root-file ROOT_FILE
                        If specified use hadd to merge the root files
```


# Installation for Event Index
1. ```python setup.py install```
2. Make sure you have LHCbDIRAC running the GridCollectorAgent shipped with the package