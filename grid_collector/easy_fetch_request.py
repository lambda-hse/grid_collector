#!/bin/env python2
import os
import argparse
import logging
import subprocess

from grid_collector.request import LFNsRequest
from grid_collector.lfn_resolver import LFNResolver, normalize_lfns
from grid_collector.request_status import RequestStatus
from grid_collector.collector import merge_root, move_and_index_jsons, cleanup


def main():
    parser = argparse.ArgumentParser(
        description="Fetches a request from GRID")
    parser.add_argument("request", type=str, help="Event Index generated request file"
                        "in json format")
    parser.add_argument("tmp_folder", type=str, help=
                        "folder for storing temporary files.")
    parser.add_argument("-c", "--cleanup", action="store_true",
                        help="Clean the temporary files")
    parser.add_argument("-n", "--n-jobs", type=int, default=1,
                        help="number of parallel jobs to launch")
    actions = parser.add_argument_group('actions')
    actions.add_argument("-j", "--json-folder", type=str,
                        help="If specified, move EventDisplay json files"
                        " to the folder")
    actions.add_argument("-u", "--url-prefix", type=str, default='',
                        help="Add this prefix to the json files listing. Meaningless"
                        " without -j")
    actions.add_argument("-r", "--root-file", type=str,
                        help="If specified use hadd to merge the root files")
    args = parser.parse_args()
    if not os.path.exists(args.tmp_folder):
        os.mkdir(args.tmp_folder)
    request = LFNsRequest()
    request.load_from_json(args.request)
    lfn2pfn_map = get_lfn2pfn_map(request.get_lfn_list())
    lfn_resolver = LFNResolver()
    lfn_resolver.update_from_dict(lfn2pfn_map)
    lfn_map_file = os.path.join(args.tmp_folder, "lfn_map.json")
    lfn_resolver.dump_to_json(lfn_map_file)
    da_vinci_environment = getProjectEnvironment("", 'DaVinci --dev v37r2p3', 'gfal CASTOR lfc')
    request_status_file = os.path.join(args.tmp_folder, "request_status.json")
    process = subprocess.Popen(("run_process_request.py",
                                args.request, lfn_map_file,
                                request_status_file, tmp_folder, '-n', str(args.n_jobs)),
                               env = da_vinci_environment['Value'])
    return_code = process.wait()
    if return_code != 0:
        raise RuntimeError("run_process_request.py exited with non-zero code")
    request_status = RequestStatus()
    request_status.update_from_json(request_status_file)
    if args.json_folder:
        move_and_index_jsons(request_status, args.tmp_folder, args.json_folder, args.url_prefix)
    if args.root_file:
        merge_root(request_status, args.tmp_folder, args.root_file, da_vinci_environment['Value'])
    print(request_status.get_human_readable_status())
    if args.cleanup:
        cleanup(request_status, args.tmp_folder)
        os.remove(request_status_file)
        os.remove(lfn_map_file)


if __name__ == '__main__':
    main()
