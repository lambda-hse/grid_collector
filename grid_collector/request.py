"""A class for holding requests for events in form of LNFs and
positins in them

"""

import cPickle
from collections import namedtuple
from itertools import imap
import json

SingleLFNRequest = namedtuple("SingleLFNRequest",
                              ['lfn', 'positions', 'cond_db'])

class LFNsRequest(object):
    """A class for holding a request in form of LNFs and positins in them

    """

    def __init__(self):
        self.requests_dict = dict()
    
    def load_from_pickle(self, file_name):
        """Loads state from a pickled dict, see
        update_from_dict for format.
        """
        with open(file_name) as input_file:
            self.requests_dict = cPickle.load(input_file)
            
    def load_from_json(self, file_name):
        """Loads state from a json, see
        update_from_dict for format.
        """
        with open(file_name) as input_file:
            self.requests_dict = json.load(input_file)
        
    def dump_to_json(self, file_name):
        """Saves self state into a json, suitable for loading with
        load_from_json

        """
        with open(file_name, 'w') as output_file:
            json.dump(self.requests_dict, output_file)
    
    def update_from_dict(self, dict_):
        """Updates self state from a dict. Warning: if a request for an LFN
        is present both in dict_ and self state, it will be overwritten, not merged.

        Args:
          dict_: {"<LFN>": {"positions": [<list_of_positions>], "cond_db": {<cond_db_flags>}}, ...}
          if you intend to use other functions in grid_collector, cond_db must contain:
          LHCBCOND, DDDB, DQFLAGS
        """
        self.requests_dict.update(dict_)

    def get_lfn_list(self):
        """Returns list of LFNs from which events are requested"""
        return self.requests_dict.keys()
        
    def single_lfn_request_iterator(self):
        """Yields requests for single LFNs in form of SingleLFNRequest - a
        namedtuple with fields lfn, positions, cond_db

        """
        for lfn, request_params in self.requests_dict.iteritems():
            yield SingleLFNRequest(
                lfn,
                map(int, request_params['positions']),
                dict(imap(lambda (key, value):
                          (key, str(value)), request_params['cond_db'].iteritems())))
