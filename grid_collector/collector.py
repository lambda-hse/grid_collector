#!/bin/env python2
"""A collection of functions to collect requests fetched bby
process_request for Event Index"""

from itertools import imap, ifilter, chain
import os
import subprocess
import shutil
import glob
import sys
import argparse
import json
import uuid

from request_status import RequestStatus


def merge_root(request_result, request_folder, root_file, environment):
    """Merges .root files with hadd.

    Args:
      request_result: a grid_collector.request_status.RequestStatus instance
      request_folder: folder with jobs result from process_request
      root_file: target .root file name to merge into
      environment: environment for running hadd.
    """
    root_files = ifilter(os.path.isfile, imap(
        lambda uuid: os.path.join(request_folder, uuid, "ei_request.root"),
                     request_result.get_successful_lfns_uuids()))
    return_code = subprocess.call(chain(["hadd", "-f6", root_file], root_files),
                                  env=environment)
    if return_code != 0:
        raise RuntimeError("hadd failed with code %d" % return_code)
    

def move_and_index_jsons(request_result, request_folder, json_path, url_prefix):
    """Moves .json files from the successful jobs folders to the
    target folder. Produces a listing with urls.

    Args:
      request_result: a grid_collector.request_status.RequestStatus instance
      request_folder: folder with jobs result from process_request
      json_path: folder to move .json files into
      url_prefix: urls in the listing are in the form
        <url_prefix>/<json_file_name>
    """
    if not os.path.exists(json_path):
        os.makedirs(json_path)
    for uuid in request_result.get_successful_lfns_uuids():
        for file_ in glob.glob(os.path.join(request_folder, uuid, "*.json")):
            file_name = os.path.basename(file_)
            target_name = os.path.join(json_path, file_name)
            # A hack to get around streams containing event with same
            # run_number.event_number
            if os.path.exists(target_name):
                target_name = os.path.join(json_path,
                                           "%s_%s" % (str(uuid.uuid4), file_name))
            shutil.move(file_, target_name)
    json_files = map(lambda file_: url_prefix + "/" + file_, ifilter(
        lambda file_: file_.endswith('.json'), os.listdir(json_path)))
    with open(os.path.join(json_path, 'events.json'), 'w') as listing_file:
        json.dump(json_files, listing_file)

def cleanup(request_result, request_folder):
    """Removes the successful jobs folders.

    Agrs:
      request_result: a grid_collector.request_status.RequestStatus instance
      request_folder: folder with jobs result from process_request
    """
    job_folders = imap(lambda uuid: os.path.join(request_folder, uuid),
                       request_result.get_successful_lfns_uuids())
    for job_folder in job_folders:
        shutil.rmtree(job_folder)

def main():
    parser = argparse.ArgumentParser(
        description="Collects processed request results for EventIndex")
    parser.add_argument("status_file", type=str,
                        help="request status file")
    parser.add_argument("request_folder", type=str)
    parser.add_argument("-c", "--cleanup", action="store_true",
                        help="Clean the job folders after the merge")
    parser.add_argument("-j", "--json-folder", type=str,
                        help="If specified, move EventDisplay json files"
                        " to the folder")
    parser.add_argument("-u", "--url-prefix", type=str, default='',
                        help="Add this prefix to the json files listing. Meaningless"
                        " without -j")
    parser.add_argument("-r", "--root-file", type=str,
                        help="If specified use hadd to merge the root files")
    args = parser.parse_args()
    request_status = RequestStatus()
    request_status.update_from_json(args.status_file)
    if args.json_folder:
        move_and_index_jsons(request_status, args.request_folder, args.json_folder, args.url_prefix)
    if args.root_file:
        merge_root(request_status, args.request_folder, args.root_file, None)
    if args.cleanup:
        cleanup(request_status, args.request_folder)
    return 0
    
    
if __name__ == '__main__':
    sys.exit(main())
