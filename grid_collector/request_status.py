import json
from collections import defaultdict
from itertools import ifilter, imap

def prepare_dict():
    return {"success": {}, "failure": {}}

class RequestStatus(object):
    """Holds status for a LFNsRequest processing: what LFNs were tried,
    which jobs on what storage elements were successful.

    """
    def __init__(self):
        self.status_dict = defaultdict(prepare_dict)

    def update_from_request_status(self, other_request_status):
        """Updates self form another RequestStatus. Warning: if a request for
        an LFN is present both in other and self, it will be
        overwritten, not merged.

        """
        self.status_dict.update(other_request_status.status_dict)

    def add_attempt(self, lfn, storage_element, uuid, success):
        """Report a processing attempt.

        Args:
          lfn: str, LFN which processing was tried
          storage_element: str, storage element on which processing was tried
          uuid: str, job uuid
          success: bool, whether the attempt was successful
        """
        if success:
            path = 'success'
        else:
            path = 'failure'
        self.status_dict[lfn][path][storage_element] = uuid
    
    def get_lfn_count(self):
        """Get the total number of LFNs on which attempts were made.

        """
        return len(self.status_dict)

    def get_successful_lfns(self):
        """Get list of LFNs for which at least one successful attempt was
        made

        """
        
        return map(lambda (lfn, status): lfn, filter(
            lambda (lfn, status): len(status['success']) != 0,
                          self.status_dict.iteritems())) 

    def get_failed_lfns(self):
        """Get list of LFNs for which at least one failed and no successfully
        attempts were made.

        """
        return map(lambda (lfn, status): lfn, filter(
            lambda (lfn, status): len(status['success']) == 0,
            self.status_dict.iteritems())) 
        
    def get_human_readable_status(self):
        """Get status in human-readable form."""
        failed_lfns = self.get_failed_lfns()
        if len(failed_lfns) == 0:
            return "Request is entirely successful"
        lfn_count = self.get_lfn_count()
        if len(failed_lfns) == lfn_count:
            return "All LFNs failed"
        return "Failed %d LFNs out of %d, first 10:\n%s" % (
            len(failed_lfns), lfn_count, '\n'.join(failed_lfns[:10]))

    def dump_to_json(self, file_name):
        """Save self state to a json.

        Args:
          file_name: str, file name to save into"""
        with open(file_name, 'w') as output_file:
            json.dump(self.status_dict, output_file)

    def update_from_json(self, file_name):
        """Updates self from json written by dump_to_json. Warning: if a
        request for an LFN is present both in other and self, it will
        be overwritten, not merged.

        """
        with open(file_name) as input_file:
            self.status_dict.update(json.load(input_file))
    
    def get_successful_lfns_uuids(self):
        """For each LFN, where at least one job was successful, yield uuid for
        some successful job.

        """
        
        for results in self.status_dict.itervalues():
            if len(results['success']) > 0:
                yield results['success'][results['success'].keys()[0]]

    def get_successful_lfns_count(self):
        """Get count of LFNs for which at least one successful attempt was
        made

        """
        return sum(imap(lambda results: int(len(results['success']) > 0),
                        self.status_dict.itervalues()))
