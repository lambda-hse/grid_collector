#!/bin/sh

if [ "root" != "$(whoami)" ]
then
    echo "This script must be started by root"
    exit 1
fi

WAIT=1
[ "$1" = "nowait" ] && WAIT=0

# unpolite restart
pkill -f -9 server.py

while [ $WAIT = 1 ]
do
    echo "Waiting for cron to start a service"
    pgrep -f server.py > /dev/null && break
    sleep 5
done

echo "Done (wait = $WAIT)"
