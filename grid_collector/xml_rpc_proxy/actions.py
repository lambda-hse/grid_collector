import re
import os
import sys
import uuid
import logging

from grid_collector.status_db import StatusDB
from grid_collector.request import LFNsRequest

logger = logging.getLogger('Log')

class EventIndexRPC(object):
    def __init__(self, config):
        self.status_db = StatusDB(config['StatusDB_file_name'])
        self.downloads_base_url = config['downloads_base_url']
        self.requests_folder = config["requests_folder"]
        

    def get_url(self, request_uuid):
        return "%s/%s.root" % (self.downloads_base_url, request_uuid)

    def download(self, request_dict, email=None):
        logger.debug("REQ DOWNLOAD: email=%s" % (email))
        request_uuid = str(uuid.uuid4())
        request = LFNsRequest()
        request.update_from_dict(request_dict)
        request.dump_to_json(os.path.join(self.requests_folder, "%s.json" % request_uuid))
        self.status_db.set_status(request_uuid, self.status_db.STATUS_NEW, "Just arrived")
        return request_uuid

    def is_ready(self, download_uuid):
        """
        Get download status.

        I don't know exactly how queries are processed by xmlrpc, and
        if I am to lock reading/writings to file.

        @param download_uuid: uuid for download (see doc for "download")
        @return: status for download
        @rtype: basestring
        """
        logger.debug("REQ IS_READY: uuid=%s" % (download_uuid))
        download_uuid = str(download_uuid)
        if not re.match('[A-Za-z0-9-]+', download_uuid):
            logger.debug("invalid ID")
            return {
                'status':  self.status_db.STATUS_INVALID,
                'details': "invalid ID"
            }
        request_status = self.status_db.get_status(download_uuid)
        return {
            'status': request_status.short,
            'details': request_status.long
        }
