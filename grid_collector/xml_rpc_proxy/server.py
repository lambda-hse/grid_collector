#!/usr/bin/env python
"""
    creates XMLRPCServer instance that listens for download requests
"""

import os
import re
import sys
import logging
import subprocess
import json
from SimpleXMLRPCServer import SimpleXMLRPCServer, SimpleXMLRPCRequestHandler

from actions import EventIndexRPC
import argparse


class LoggingSimpleXMLRPCRequestHandler(SimpleXMLRPCRequestHandler):
    """Overides the default SimpleXMLRPCRequestHander to support logging.  Logs
    client IP and the XML request and response.
    """

    def __init__(self, request, client_address, server):
        self.logger = logging.getLogger('Log')
        SimpleXMLRPCRequestHandler.__init__(self, request, client_address, server)

    def log_request(self, code='-', size='-'):
        if self.server.logRequests:
            clientIP, port = self.client_address
            # data = self.rfile.read(int(self.headers["content-length"]))
            self.logger.info('From: %s, %s. CMD: %s. PATH: %s' %
                            (clientIP, port, self.command, self.path))
            # self.logger.debug('Client request: \n%s\n' % self.headers)
            # SimpleXMLRPCRequestHandler.log_request(self, code, size)


def is_already_running(pid_file):
    """ Checks, if a service is already started.

        Checks contents of a `pid_file', it contains the PID for the
        last started service. It a process with this PID already exists,
        returns True, otherwise False.

        `pid_file' if None, is taken from the default value PID_FILE.
        You may need to override this for tests.

    """
    pid_file_res = pid_file
    if not os.path.exists(pid_file_res):
        return False
    with open(pid_file_res, 'r') as pid_file_handler:
        data = pid_file_handler.read()
        if not data.strip():
            return False
        pid = int(data)
    ps_process = subprocess.Popen(['ps', 'ax'], stdout=subprocess.PIPE)
    output = ps_process.communicate()[0]
    return re.search(r'(^|\s){pid}\s.*server'.format(pid=pid),
                     output) is not None


def create_pid_lock(pid_file):
    """ Writes current PID to pid_file.

       `pid_file' if None, is taken from the default value PID_FILE.
        You may need to override this for tests.

    """
    with open(pid_file, 'w') as pid_file_handler:
        pid_file_handler.write(str(os.getpid()))


def main():
    parser = argparse.ArgumentParser(
        description='run server for passing requests to SimpleAgent')
    parser.add_argument("config_file", type=argparse.FileType('r'),
                        help="configuration file")
    parser.add_argument(
        '--log', default=sys.stdout, type=argparse.FileType('w'),
        help='the file where the sum should be written')
    args = parser.parse_args()
    config = json.load(args.config_file)
    if is_already_running(config['rpc_pid_file']):
        return
    create_pid_lock(config['rpc_pid_file'])
    logger = logging.getLogger('Log')
    hdlr = logging.StreamHandler(args.log)
    formatter = logging.Formatter("%(asctime)s  %(levelname)s  %(message)s")
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(config['log_level'])

    server = SimpleXMLRPCServer(
        (config['rpc_listen_address'], config['rpc_listen_port']),
         LoggingSimpleXMLRPCRequestHandler)
    event_index_rpc = EventIndexRPC(config)
    server.register_introspection_functions()
    server.register_function(event_index_rpc.download)
    server.register_function(event_index_rpc.is_ready)
    server.serve_forever()


if __name__ == '__main__':
    main()
