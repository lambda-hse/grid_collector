"""GaudiPython-based event processing.

Processes events with GaudiPython for Event Index. Copies
to a .root file file and produces .json files.

Usage:
1. Get proper DaVinci environment, make sure you include JsonConverter
  algorithm.
2. Run process_pfn with multiprocessing.Process. Warning!
  Running not in a separate process will leave you with a spoiled global state.
"""

import os
import logging
from itertools import imap
import sys
import io

from multiprocessing import Manager


def process_pfn(PFN, positions, cond_db, folder):
    """Runs processig specified in configure_da_vinci_algorithms
       on events in the given PFN on given positions.

    Args:
      PFN: str, PFN with the target events
      positions: list with events position in the PFN
      cond_db: dict with CondDB parameters for the file, must have:
        LHCBCOND, DDDB, DQFLAGS
      folder: folder to write the resulting files
    
    Returns:
      Nothing. However, it calls sys.exit. If return code is non-zero,
      something went wrong. If 0 the processing was successful.
    
      A successful run will produce following files:
        ei_request.root
        <run_number>_<event number>.json for each event
    
    Warning!
     This function is supposed to be run as a subprocess
     so we can avoid dealing with the pesky global state of GaudiPython
    """
    os.chdir(folder)
    logging.info("grid_wizard job starting, folder %s" % folder)
    from Configurables import DaVinci
    import GaudiPython
    logging.debug("GaudiPython and DaVinci imported")
    da_vinci = DaVinci()
    configure_da_vinci_conditions(da_vinci, cond_db)
    algorithms, required_event_containers = configure_da_vinci_algorithms(da_vinci)
    logging.debug("DaVinci configured, running AppMgr")
    app_mgr = GaudiPython.AppMgr()
    success = run_app_mgr(app_mgr, algorithms, PFN, positions, required_event_containers)
    app_mgr.stop()
    app_mgr.exit()
    if success:
        sys.exit(0)
    else:
        sys.exit(1)
    

def configure_da_vinci_algorithms(da_vinci):
    """Configures DaVinci for the desired actions.

    Args:
      da_vinci - a DaVinci configurable
    
    By default, two algorithms are configured:
      1. InputCopyStream - copies events to ei_request.root
      2. JsonConverter - writes events to <run_number>_<event number>.json
   
    Returns:
      (<list_of_DaVinci_algorithms>, <required_event_containers>)
      where required_event_containers is a tuple of
      paths that must be present in an event for
      successful invocation of the algorithms
    """
    from Configurables import InputCopyStream, JsonConverter
    # Your code goes here
    # write and configure the algorithms
    # you can (and probably should) use
    # relative paths as each invocation
    # of grid wizard is done in a separate
    # folder
    input_copy_stream = InputCopyStream()
    input_copy_stream.Output = \
        "DATAFILE='PFN:%s' TYP='POOL_ROOTTREE' OPT='REC' " % "ei_request.root"
    json_converter = JsonConverter()
    json_converter.OutputDirectory = "."

    da_vinci.UserAlgorithms = [json_converter, input_copy_stream]
    algorithms = ('InputCopyStream', 'JsonConverter')

    # Your code goes here:
    # If those paths are not present the event is
    # considered invalid
    required_event_containers = (
        'Rec/Header',
    )
    return algorithms, required_event_containers


def configure_da_vinci_conditions(da_vinci, cond_db):
    """Configures DaVinci with CondDB information
      from cond_db.

    Args:
      da_vinci: a DaVinci configurable
      cond_db: dict with CondDB parameters for the file, must have:
        LHCBCOND, DDDB, DQFLAGS
    """
    da_vinci.DataType = "2012"
    if ("LHCBCOND" in cond_db) and ("SIMCOND" in cond_db):
        raise ValueError("Both SIMCOND and LHCBCOND are present")
    elif (not ("LHCBCOND" in cond_db)) and (not ("SIMCOND" in cond_db)):
        raise ValueError("Neither SIMCOND nor LHCBCOND are present")
    elif "LHCBCOND" in cond_db:
        da_vinci.Simulation = False
        da_vinci.CondDBtag = cond_db["LHCBCOND"]
    else:
        da_vinci.Simulation = False
        da_vinci.CondDBtag = cond_db["SIMCOND"]
    da_vinci.DDDBtag = cond_db["DDDB"]
    da_vinci.DQFLAGStag = cond_db["DQFLAGS"]
  

def run_app_mgr(app_mgr, algorithms, PFN, positions, required_event_containers):
    """Runs an AppMgr instance, skips non-target events.

    Args:
      app_mgr: a GaudiPython.AppMgr instance
      algorithms: a list of algorithms in GaudiPython.AppMgr
        to run on target events. Warning! If you configure
        some algorithms in the AppMgr, they will not get disabled
        and, therefore, will be run on every event in the PFN,
        not only the target ones.
      PFN: str, PFN with the target events
      positions: list with events position in the PFN
      required_event_containers: a tuple of
        paths that must be present in an event for
        successful invocation of the algorithms. If one of
        them is not present in an event, the run
        is aborted with an error.
    
    Returns:
       True if the run was successful, False otherwise
    """
    evt = app_mgr.evtsvc()
    for algorithm in algorithms:
        app_mgr.algorithm(algorithm).enable = False
    logging.info("Opening PFN: %s" % PFN)
    app_mgr.evtsel().open([PFN])
    current_position = 0
    for pos in sorted(positions):
        offset = pos - current_position
        assert offset >= 0, "wrong offset: %d" % offset
        try:
            ret = app_mgr.run(offset)
            current_position += offset 
        except Exception as error:
            logging.fatal("appMgr failed with: %s" % str(error))
            return False
        if all(imap(lambda container: evt[container], required_event_containers)):
            for algorithm in algorithms:
                app_mgr.algorithm(algorithm).execute()
        else:
            logging.fatal("A required conainer missing at %d" % pos)
            return False
    return True
