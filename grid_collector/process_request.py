#!/usr/bin/env python2
"""Given a request in format LFN+positions in it
   and a LFN->PFN map fetches events into .root
   and .json files.
"""

import os
import sys
from itertools import imap, ifilter
import logging
import multiprocessing
from multiprocessing import Process, Manager, Pool, Value, Queue, RLock
import time
import uuid
import shutil
import random

from lfn_resolver import normalize_lfns
from grid_wizard import process_pfn
from request_status import RequestStatus


class GridBandito(object):
    """A class with a simple implementation of
    the Multi-armed bandit in infinitely expandable space
    with binary payoff. Multiprocessing safe.
    Algorithm:
    1. If a new lever is added, and it has no running games, try it.
    2. With given probability try the least explored lever -
       with one with the least total number of running and finished games.
    3. Return the lever with the greatest win rate.
    """
    def __init__(self, exploration_probability):
        """
        Args:
          exploration_probability: probability of trying the least explored lever.
        """
        self.manager = Manager()
        self.state_dict = self.manager.dict()
        self.mutex = RLock()
        self.exploration_probability = exploration_probability

    def get_value(self, values):
        """Query the bandit for a value. It will then mark it as "running",
        so after finishing processing, you want to report_result.

        Args:
          values: list of values for bandit to select from. A value
            can be anything storable in python dict.
        """
        # First, we check whether we encounter a new value
        # If so, we return it
        if len(values) == 0:
            raise ValueError("Bandito can't select from an empty set")
        self.mutex.acquire()
        new_values = set(values).difference(self.state_dict.keys())
        if len(new_values) > 0:
            selection = random.choice(list(new_values))
            self.state_dict[selection] = {
              'succeeded': 0,
              'failed': 0,
              'running': 1
            }
            self.mutex.release()
            return selection
        candidates = zip(values, imap(self.state_dict.__getitem__, values)) 
        explored_states = filter(lambda (key, state):
                                 state['succeeded'] + state['failed'] > 0,
                                 candidates)
        # If we roll for exploration or have no explored states, we
        # return the value with the less attempts,
        # both running and finished
        if (len(explored_states) == 0 or random.random() < self.exploration_probability):
            attempts = map(lambda (key, state): (key, sum(state.itervalues())),
                           candidates)
            min_attempts = min(attempts, key=lambda attempt: attempt[1])[1]
            selection = random.choice(filter(
              lambda attempt: attempt[1] == min_attempts, attempts))[0]
        else:
            # Return the least loaded site among the ones with the
            # best success ratio
            success_ratios = map(lambda (key, state):
                                  (key, float(state['succeeded']) /
                                   (state['succeeded'] + state['failed'])),
                                   explored_states)
            logging.info("Bandito ratios: %s\nExplored states: %s" % (
                str(success_ratios), str(explored_states)))
            best_rate = max(success_ratios, key=lambda attempt: attempt[1])[1]
            logging.info("Bandito best rate %f" % best_rate)
            selection = random.choice(filter(
              lambda attempt: attempt[1] == best_rate, success_ratios))[0]
        selection_status = self.state_dict[selection]
        selection_status['running'] += 1
        self.state_dict[selection] = selection_status
        self.mutex.release()
        return selection
        
    def report_result(self, value, success):
        """Report a game result to the bandito.

        Args:
           value: lever that was pulled. Should be gotten
            from get_value, as Bandito keeps track of the running games.
           success: a bool, whether the game was successful
        """
        if value not in self.state_dict:
            raise ValueError("Unknown value reported to Bandito")
        self.mutex.acquire()
        value_status = self.state_dict[value]
        value_status['running'] -= 1
        if success:
            value_status['succeeded'] += 1
        else:
            value_status['failed'] += 1
        self.state_dict[value] = value_status
        self.mutex.release()


def process_lfn(lfn_request, lfn_resolver, preserve_failed_jobs, return_queue, grid_bandito,
                request_folder):
    """Processes an LFN for Event Index, designed for multiprocessing usage.

    Downloads a request for Event Index. Tries different PFNs into
    folders request_folder/<uuid.uuid4()> until it succeeds or runs
    ont of PFNs, pushes a RequestStatus instance into the
    return_queue.

    Args:
       lfn_request: a grid_collector.request.SingleLFNRequest instance
       lfn_resolver: a grid_collector.lfn_resolver.LFNResolver instance
       preserve_failed_jobs: bool, if False, will remove folders after failed jobs
       return_queue: multiprocessing.Queue to push the resulting RequestStatus
       grid_bandito: a GridBandito instance
       request_folder: folder to run the jobs in
    """
    request_status = RequestStatus()
    successfuly_processed_lfn = False
    se_pfns = lfn_resolver.resolve_lfn(normalize_lfns([lfn_request.lfn, ])[0])
    positions = lfn_request.positions
    ses = se_pfns.keys()
    while len(ses) > 0:
        SE = grid_bandito.get_value(ses)
        logging.debug("Got SE %s from bandito" % SE)
        ses.remove(SE)
        PFNs = se_pfns[SE]
        for pfn in PFNs:
            job_uuid = str(uuid.uuid4())
            job_folder = os.path.join(request_folder, job_uuid)
            os.mkdir(job_folder)
            logging.debug("Calling grid_wizard for PFN %s, uuid %s" % (
                pfn, job_uuid))
            pfn_process = Process(target=process_pfn, args=(
              pfn, positions, lfn_request.cond_db, job_folder))
            pfn_process.start()
            pfn_process.join()
            successfuly_processed_lfn = (pfn_process.exitcode == 0)
            request_status.add_attempt(lfn_request.lfn, SE, job_uuid, successfuly_processed_lfn)
            if successfuly_processed_lfn:
                break
            logging.warning("PFN %s failed" % (pfn))
            if not preserve_failed_jobs:
                shutil.rmtree(job_folder)
        grid_bandito.report_result(SE, successfuly_processed_lfn)
        if successfuly_processed_lfn:
            break
    if not successfuly_processed_lfn:
        logging.error("Failed to process LFN %s" % lfn_request.lfn)
    return_queue.put(request_status.status_dict)
    return 0


def wait_pool(pool):
    """Given a list of multiprocessing.Process waits for one of them to
    finish, removes it from the list and returns it.

    """
    WAIT_TIME = 0.05
    while True:
        for index, process in enumerate(pool):
            if not process.is_alive():
                return pool.pop(index)
        time.sleep(WAIT_TIME)
      

def process_request(request, lfn_resolver, preserve_failed_jobs, n_jobs,
                    request_folder):
    """Processes request in format LFN+positions to into .root (one per
    LFN) and .json (one per event) files. The files are stored in
    request_folder/<uuid.uuid4()> and the uuids are returned as a
    grid_collector.request_status.RequestStatus

    Args:
      request: grid_collector.request.LFNsRequest
      lfn_resolver: a grid_collector.lfn_resolver.LFNResolver instance
      preserve_failed_jobs: bool, if False, will remove folders after failed jobs
      n_jobs: maximum number of parallel jobs. A job is processing of an LFN.
      request_folder: folder to run the jobs in
    
    Returns:
      grid_collector.request_status.RequestStatus for each lfn containing
      succesful and failed jobs.
    """
    request_status = RequestStatus()
    active_jobs = 0
    processes = []
    results_queue = Queue()
    bandito = GridBandito(0.05)
    for lfn_request in request.single_lfn_request_iterator():
        if active_jobs == n_jobs:
            wait_pool(processes)
            active_jobs -= 1
        processes.append(Process(target=process_lfn, args=(
            lfn_request, lfn_resolver, preserve_failed_jobs,
            results_queue, bandito, request_folder)))
        processes[-1].start()
        active_jobs += 1
        while not results_queue.empty():
            request_status.status_dict.update(results_queue.get_nowait())
    while len(processes) > 0:
        wait_pool(processes)
        while not results_queue.empty():
            request_status.status_dict.update(results_queue.get_nowait())
    while not results_queue.empty():
        request_status.status_dict.update(results_queue.get_nowait())
    logging.info(str(bandito.state_dict))
    return request_status

